package utils;

import java.io.File;
import java.util.HashMap;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServerHasNotBeenStartedLocallyException;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public class ServerManager {

    private static ThreadLocal<AppiumDriverLocalService> server = new ThreadLocal<>();
    TestUtils utils = new TestUtils();

    public AppiumDriverLocalService getServer(){
        return server.get();
    }

    public void startServer() {
        utils.log().info( "Starting appium server..." );

        AppiumDriverLocalService server = windowsGetAppiumSevice();
        server.start();
        // if( server.isRunning() ){
        //     utils.log().info( "SERVER STARTED" );
        // }
        if( server == null || !server.isRunning() ) {
            utils.log().fatal( "Appium server not started1. ABORT!!!" );
            throw new AppiumServerHasNotBeenStartedLocallyException( "Appium server not started. ABORT!!!" );
        }
        // server.clearOutPutStreams();

        ServerManager.server.set( server );
        utils.log().info( "Appium server started" );
    }

    public AppiumDriverLocalService getAppiumserverDefault() {
        return AppiumDriverLocalService.buildDefaultService();
    }

    public AppiumDriverLocalService windowsGetAppiumSevice() {
        GlobalParams params = new GlobalParams();

        AppiumDriverLocalService service = AppiumDriverLocalService.buildService( new AppiumServiceBuilder()
                .usingAnyFreePort()
                .withArgument( GeneralServerFlag.SESSION_OVERRIDE )
                .withLogFile( new File( params.getPlatformName() + "_" + params.getDeviceName() + File.separator + "Server.log" ) )
        );

        return service;
    }

    public AppiumDriverLocalService macGetAppiumSevice(){
        GlobalParams params = new GlobalParams();
        HashMap<String, String> environment = new HashMap<>();
        environment.put( "PATH", "" );
        environment.put( "ANDROID_HOME", "" );
        environment.put( "JAVA_HOME", "" );

        return AppiumDriverLocalService.buildService( new AppiumServiceBuilder()
                .usingDriverExecutable( new File( "" ) )
                .withAppiumJS( new File  ("" ) )
                .usingAnyFreePort()
                .withArgument( GeneralServerFlag.SESSION_OVERRIDE )
                .withEnvironment( environment )
                .withLogFile( new File( params.getPlatformName() + "_" + params.getDeviceName() + File.separator + "Server.log" ) )
        );
    }
}
