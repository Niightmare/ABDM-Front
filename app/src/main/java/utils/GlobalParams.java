package utils;


public class GlobalParams {
    private static ThreadLocal<String> platformName = new ThreadLocal<String>();
    private static ThreadLocal<String> udid = new ThreadLocal<String>();
    private static ThreadLocal<String> deviceName = new ThreadLocal<String>();
    private static ThreadLocal<String> systemPort = new ThreadLocal<String>();
    private static ThreadLocal<String> chromeDriverPort = new ThreadLocal<String>();
    private static ThreadLocal<String> wdaLocalPort = new ThreadLocal<String>();
    private static ThreadLocal<String> webkitDebugProxyPort = new ThreadLocal<String>();

    public void setPlatformName ( String platformName ){
        GlobalParams.platformName.set( platformName );
    }

    public String getPlatformName () {
        return platformName.get();
    }

    public void setUDID ( String udid ){
        GlobalParams.udid.set( udid );
    }

    public String getUDID () {
        return udid.get();
    }

    public void setDeviceName ( String deviceName ) {
        GlobalParams.deviceName.set( deviceName );
    }

    public String getDeviceName () {
        return deviceName.get();
    }

    public void setSystemPort ( String systemPort ) {
        GlobalParams.systemPort.set( systemPort );
    }

    public String getSystemPort () {
        return systemPort.get();
    }

    public void setChromeDriverPort ( String chromeDriverPort ) {
        GlobalParams.chromeDriverPort.set( chromeDriverPort );
    }

    public String getChromeDriverPort () {
        return chromeDriverPort.get();
    }

    public void setWdaLocalPort ( String wdaLocalPort ) {
        GlobalParams.wdaLocalPort.set( wdaLocalPort );
    }

    public String getWdaLocalPort () {
        return wdaLocalPort.get();
    }

    public void setWebkitDebugProxyPort ( String webkitDebugProxyPort ) {
        GlobalParams.webkitDebugProxyPort.set( webkitDebugProxyPort );
    }

    public String getWebkitDebugProxyPort () {
        return webkitDebugProxyPort.get();
    }

    public void initializeGlobalParams ( String platformName, String udid, String deviceName ) {
        GlobalParams params = new GlobalParams(); 
        params.setPlatformName( System.getProperty( "platformName", platformName ) );
        params.setDeviceName( System.getProperty( "deviceName", deviceName ) );
        params.setUDID( System.getProperty( "udid", udid ) );

        switch( params.getPlatformName() ){
            case "Android" :
                params.setSystemPort(  System.getProperty( "systemPort", "10000" ) );
                params.setChromeDriverPort( System.getProperty( "ChromeDriverPort", "11000" ) );
                break;
            case "iOS" :
                params.setWdaLocalPort(  System.getProperty( "WdaLocalPort", "10001" ) );
                params.setWebkitDebugProxyPort ( System.getProperty( "WebkitDebugProxyPort ", "11001" ) );
                break;
            default:
                throw new IllegalStateException( "Invalid platform name!" );
        }
    }

    
}
