package utils;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class TestUtils {

	public Logger log() {
		return LogManager.getLogger( Thread.currentThread().getStackTrace()[2].getClassName() );
	}

}
