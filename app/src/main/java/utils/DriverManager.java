package utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import java.io.IOException;
import java.net.URL;

public class DriverManager {

    private static ThreadLocal<AppiumDriver> driver = new ThreadLocal<>();
    TestUtils utils = new TestUtils();


    public AppiumDriver getDriver(){
        return driver.get();
    }

    public void setDriver( AppiumDriver driver ) {
        DriverManager.driver.set( driver );
    }

    public void initializeDriver () throws Exception {
        AppiumDriver driver = null;
        GlobalParams params = new GlobalParams();
        PropertyManager properties = new PropertyManager();

        if( driver == null ){
            try {
                utils.log().info( "Initializing appium driver" );

                switch ( params.getPlatformName() ) {
                    case "Android" :
                        // Levanta su propio appium server
                        driver = new AndroidDriver( new ServerManager().getServer().getUrl(), new CapabilitiesManager().getCapabilities() );

                        // Necesitas tener levantado appium server para que se ejecute
//                        driver = new AndroidDriver( new URL( properties.getProperties().getProperty( "appiumURL" ) ) , new CapabilitiesManager().getCapabilities() );
                        utils.log().info("DRIVER: " + driver);


                        break;
                    case "iOS" :
                        driver = new IOSDriver( new ServerManager().getServer().getUrl(), new CapabilitiesManager().getCapabilities() );
                        utils.log().info("DRIVER: " + driver);
                        break;
                }

                if( driver ==null ){
                    throw new Exception( "Driver is null...ABORTING!" );
                }
                utils.log().info( "Driver is initialized..." );
                DriverManager.driver.set( driver );

            } catch ( IOException e ) {
                e.printStackTrace();
                utils.log().fatal( "Driver initialization failure" + e.toString() );
                throw e;
            }
        }
    }


}
