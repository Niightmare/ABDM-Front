package utils;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.remote.MobileCapabilityType;

public class CapabilitiesManager {
    TestUtils utils = new TestUtils();

    public DesiredCapabilities getCapabilities() throws IOException {
        GlobalParams params = new GlobalParams();
        Properties properties = new PropertyManager().getProperties();

        try {
            utils.log().info( "Getting capabilities..." );
            DesiredCapabilities capability = new DesiredCapabilities();
            capability.setCapability( MobileCapabilityType.PLATFORM_NAME, params.getPlatformName() );
            capability.setCapability( MobileCapabilityType.UDID, params.getUDID() );
            capability.setCapability( MobileCapabilityType.DEVICE_NAME, params.getDeviceName() );

            switch ( params.getPlatformName() ) {
                case "Android" :
                    capability.setCapability( "platformName", properties.getProperty( "AndroidPlatformName" ) );
                    capability.setCapability( "automationName", properties.getProperty( "androidAutomationName" ) );
                    capability.setCapability( "appPackage", properties.getProperty( "androidAppPackage" ) );
                    capability.setCapability( "appActivity", properties.getProperty( "androidAppActivity" ) );
                    //capability.setCapability( "systemPort", params.getSystemPort() );
                    //capability.setCapability( "chromeDriverPort", params.getChromeDriverPort() );

                    String androidAppURL = System.getProperty( "user.dir" ) + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "apps" + File.separator + properties.getProperty( "androidAppName" );
                    utils.log().info( "appURL is: " + androidAppURL );
                    capability.setCapability( "app", androidAppURL );
                    break;

                case "iOS" :
                    capability.setCapability( "platformName", properties.getProperty( "iOSPlatformName" ) );
                    capability.setCapability( "automationName", properties.getProperty( "iOSAutomationName" ) );
                    //capability.setCapability( "bundleId", properties.getProperty( "iOSBundleID" ) );
                    //capability.setCapability( "wdaLocalPort", params.getWdaLocalPort() );
                    //capability.setCapability( "webkitDebugProxyPort", params.getWebkitDebugProxyPort() );

                    String iOSAppURL = System.getProperty( "user.dir" ) + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "apps" + File.separator + properties.getProperty( "iOSAppName" );
                    utils.log().info( "appURL is: " + iOSAppURL );
                    capability.setCapability( "app", iOSAppURL );
                    break;
            }
            return capability;
        } catch ( Exception e ) {
            e.printStackTrace();
            utils.log().info( "Failed to load capabilities" + e.toString() );
            throw e;
        }

    }
}
