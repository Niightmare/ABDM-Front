package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyManager {
    private static Properties props = new Properties();
    TestUtils utils = new TestUtils();

    public Properties getProperties () throws IOException{
        InputStream is = null;
        String propsFileName = "config.properties";

        if( props.isEmpty() ) {
            try {
            	utils.log().info( "Loading config properties..." );
                is = getClass().getClassLoader().getResourceAsStream( propsFileName );
                props.load( is );
            } catch ( Exception e ) {
                //TODO: handle exception
                e.printStackTrace();
                utils.log().fatal( "Failed to load config properties. ABORTED!" + e.toString() );
                throw e;
            }
            finally {
                if ( is !=null ) {
                    is.close();
                }
            }
        }
        
        return props;
    }
}
