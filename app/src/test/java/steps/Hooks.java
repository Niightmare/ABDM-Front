package steps;

import io.cucumber.java.Before;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;

import utils.DriverManager;
import utils.VideoManager;

import java.io.IOException;

import org.openqa.selenium.OutputType;


public class Hooks {
    @Before
    public void initialize(){

        new VideoManager().startRecording();
    }

    @After
    public void quit( Scenario scenario ) throws IOException{

        if( scenario.isFailed() ) {
        	
            scenario.log( "Scenario failing, refer to the image attached" );
            byte[] screenshot = new DriverManager().getDriver().getScreenshotAs( OutputType.BYTES );
            scenario.attach( screenshot, "image/png", scenario.getName() );

            // byte [] screenshot = ( (TakesScreenshot) driver ).getScreenshotAs( OutputType.BYTES );
            // scenario.attach( screenshot, "image/png", "image-report" );
        }

        new VideoManager().stopRecording(scenario.getName());
        
    }
}
