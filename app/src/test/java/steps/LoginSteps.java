package steps;

import io.cucumber.java.en.*;
import pages.LoginPage;



public class LoginSteps {

    @When( "^I enter username as \"([^\"]*)\"$" )
    public void iEnterUsernameAs(String username) throws InterruptedException {
        new LoginPage().enterUserName( username );
    }

    @When( "^I enter password as \"([^\"]*)\"$" )
    public void iEnterPasswordAs( String password ) {
        new LoginPage().enterPassword( password );
    }

    @When( "^I login$" )
    public void iLogin() {
        new LoginPage().pressLoginBtn();
    }

    @Then( "^I should see Products page with title \"([^\"]*)\"$" )
    public void loginShouldFailWithAnError( String err ) {
       // Assert.assertEquals( new LoginPage().getErrTxt(), err );
    }

    

    @Given("I enter username as invalidUsername")
    public void iEnterUsernameAsInvalidUsername() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Given("I enter password as secret_sauce")
    public void iEnterPasswordAsSecretSauce() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Given("I click the login button")
    public void iClickTheLoginButton() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Then("login should fail with <error>")
    public void loginShouldFailWithError() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Given("I enter username as standar_user")
    public void iEnterUsernameAsStandarUser() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Given("I enter password as invalidPassword")
    public void iEnterPasswordAsInvalidPassword() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
}
