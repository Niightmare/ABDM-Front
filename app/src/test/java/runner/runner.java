package runner;

import org.apache.logging.log4j.ThreadContext;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import static io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE;

import utils.DriverManager;
import utils.GlobalParams;
import utils.ServerManager;



@RunWith(Cucumber.class)
@CucumberOptions(
    features = "src/test/resources/features",
    glue = "steps",
    plugin = {"pretty", "html:target/cucumber", "summary"},
    snippets = CAMELCASE,
    dryRun = true,
    // tags = "@Test", //Para pasar un solo tag
    monochrome = true
)

public class runner {
    @BeforeClass
    public static void initialize() throws Exception {
        GlobalParams params = new GlobalParams();
        //Change iOS if need to run through runner file
//        params.initializeGlobalParams( "Android", "emulator-5554", "Pixel 5 API 30" ); //PlatformName, UDID, deviceName
        params.initializeGlobalParams( "iOS", "B4824D95-8626-48EE-87E5-24884BEC14D5", "iPhone 14 Pro Max" );
        // instruments -s devices before xCode 13
        // xcrun simctl list
        ThreadContext.push( "ROUTINGKEY", params.getPlatformName() + "_" + params.getDeviceName() );

        new ServerManager().startServer();
        new DriverManager().initializeDriver();
    }

    @AfterClass
    public static void cleanDriver () {
        DriverManager driverManager = new DriverManager();
        if( driverManager.getDriver() != null ){
            driverManager.getDriver().quit();
            driverManager.setDriver( null );
        }

        ServerManager serverManager = new ServerManager();
        if( serverManager.getServer() != null ){
            serverManager.getServer().stop();
        }
    }
}
