package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.*;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import utils.DriverManager;
import utils.TestUtils;

public class BasePage {

    private AppiumDriver<?> driver;
    private static WebDriverWait wait;
    TestUtils utils = new TestUtils();

    public BasePage() {
        this.driver = new DriverManager().getDriver();
        PageFactory.initElements( new AppiumFieldDecorator( this.driver ), this );
        wait = new WebDriverWait( driver, 10 );
    }

    public void waitForVisibility( MobileElement e ) {
        wait.until( ExpectedConditions.visibilityOf( e ) );
    }

    public void waitForVisibility( By e ) {
        wait.until( ExpectedConditions.visibilityOfElementLocated( e ) );
    }

    public void clear( MobileElement e ) {
        waitForVisibility( e );
        e.clear();
    }


    public void click( MobileElement e ) {
        waitForVisibility( e );
        e.click();
    }

    public void click( MobileElement e, String msg ) {
        waitForVisibility( e );
        utils.log().info( msg );
        e.click();
    }

    public void click( By e, String msg ) {
        waitForVisibility( e );
        utils.log().info( msg );
        driver.findElement( e ).click();
    }

    public void sendKeys( MobileElement e, String txt ) {
        waitForVisibility( e );
        e.sendKeys( txt );
    }

    public void sendKeys( MobileElement e, String txt, String msg ) {
        waitForVisibility( e );
        utils.log().info( msg );
        e.sendKeys( txt );
    }

}
