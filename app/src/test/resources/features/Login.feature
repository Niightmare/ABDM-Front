Feature: Validate Login scenarios

Scenario Outline: As a user I need to validate if credentials are correct
    Given I enter username as <username>
    And I enter password as <password>
    And I click the login button
    Then login should fail with <error>

    Examples:
        | username         | password        | err                                                        |
        | invalidUsername  | secret_sauce    | Username and password do not match any user in this server |
        | standar_user     | invalidPassword | Username and password do not match any user in this server |
