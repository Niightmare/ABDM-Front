# BDD Appium

Propuesta de proyecto para la automatización front con Appium + Cucumber (BDD) basado en el curso Appium Mobile Automation - Android & iOS + Frameworks + CICD Sección 16

## Getting started

- NodeJS
- Appium CLI o Appium Desktop
- Eclipse o VSCode
- JDK
- Gradle => 7.4.1

> JAVA_HOME, JDK_HOME, GRADLE_USER_HOME, NODE_HOME deben estar previamente configuradas en las variables de entorno
